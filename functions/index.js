const functions = require("firebase-functions");
const admin = require("firebase-admin");
admin.initializeApp();
const db = admin.database();

exports.onSelectionMade = functions.database
  .ref("/config/{meetingid}/current/currentState/plugins/pipsearch/selected")
  .onWrite(async (change, context) => {
    const timestamp = Date.now();
    await db
      .ref(
        `/data/pipsearch/${context.params.meetingid}/_history/${timestamp}/_selected`
      )
      .set(change.after.val());
  });
